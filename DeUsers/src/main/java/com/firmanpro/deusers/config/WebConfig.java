package com.firmanpro.deusers.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@EnableWebMvc
@ComponentScan("com.firmanpro.deusers")
public class WebConfig {

	//mirip RootApplicationContextConfig
	
}
